//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  int value1 = 50;
  ui->horizontalSlider->setValue( value1 );
  ui->value1->setText( QString::number( value1 ) );
  //
  int value2 = 99;
  ui->verticalSlider->setValue( value2 );
  ui->value2->setText( QString::number( value2 ) );
  //
  connect( ui->horizontalSlider , &QSlider::valueChanged , this , &MainWindow::onHorizontalSliderValueChanged );
  connect( ui->verticalSlider , &QSlider::valueChanged , this , &MainWindow::onVerticalSliderValueChanged );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onHorizontalSliderValueChanged(int value)
{
  //Sync value to label
  ui->value1->setText( QString::number( ui->horizontalSlider->value() ) );
}

void MainWindow::onVerticalSliderValueChanged(int value)
{
  //Sync value to label
  ui->value2->setText( QString::number( ui->verticalSlider->value() ) );
}

