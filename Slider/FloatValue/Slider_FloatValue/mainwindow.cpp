//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  float value1 = 5.0f;
  ui->horizontalSlider->setValue( value1 * 10.0f );
  ui->label1->setText( QString::number( 0.1f * ui->horizontalSlider->value() ) );
  //
  double value2 = 0.0f;
  ui->verticalSlider->setValue( value2 * 100.0 );
  ui->label2->setText( QString::number( 0.01 * ui->verticalSlider->value() ) );
  //
  connect( ui->horizontalSlider , &QSlider::valueChanged , this , &MainWindow::onHorizontalSliderValueChanged );
  connect( ui->verticalSlider , &QSlider::valueChanged , this , &MainWindow::onVerticalSliderValueChanged );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onHorizontalSliderValueChanged(int value)
{
  //Sync value to label
  ui->label1->setText( QString::number( 0.1f * ui->horizontalSlider->value() ) );
}

void MainWindow::onVerticalSliderValueChanged(int value)
{
  //Sync value to label
  ui->label2->setText( QString::number( 0.01 * ui->verticalSlider->value() ) );
}
