//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  ui->radioButtonInGroup2->setChecked(true);
  //
  connect( ui->pushButton , &QPushButton::clicked , this , &MainWindow::onPushButton_Clicked );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onPushButton_Clicked(bool clicked)
{
  qDebug( "Radio button 1 :%s" , ui->radioButton_1->isChecked() ? "Y" : "N" );
  qDebug( "Radio button 2 :%s" , ui->radioButton_2->isChecked() ? "Y" : "N" );
  qDebug( "Radio button 3 :%s" , ui->radioButton_3->isChecked() ? "Y" : "N" );
  //
  qDebug( "Radio button in group 1 :%s" , ui->radioButtonInGroup1->isChecked() ? "Y" : "N" );
  qDebug( "Radio button in group 2 :%s" , ui->radioButtonInGroup2->isChecked() ? "Y" : "N" );
}

