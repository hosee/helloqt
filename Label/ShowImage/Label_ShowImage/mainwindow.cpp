//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  QPixmap* pPixMap=new QPixmap( "../Label_ShowImage/MyDirectory/MyImage.png" );
  ui->label1->setPixmap( *pPixMap );
  //
  QPixmap* pPixMap1=new QPixmap( ":/resImage/MyImage1.png" );
  ui->label2->setPixmap( *pPixMap1 );
  //
}

MainWindow::~MainWindow()
{
  delete ui;
}

