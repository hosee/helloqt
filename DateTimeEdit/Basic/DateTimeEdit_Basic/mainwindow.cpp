//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  connect( ui->dateTimeEdit1 , &QDateTimeEdit::dateTimeChanged , this , &MainWindow::onDateTimeEdit1dateTimeChanged );
  connect( ui->dateTimeEdit2 , &QDateTimeEdit::dateTimeChanged , this , &MainWindow::onDateTimeEdit2dateTimeChanged );

  //
  ui->dateTimeEdit1->setDate( QDate( 2020 , 6 , 6 ) );
  ui->dateTimeEdit1->setTime( QTime( 18 , 30 ) );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onDateTimeEdit1dateTimeChanged(const QDateTime &datetime)
{
  int y,m,d;
  ui->dateTimeEdit1->date().getDate(&y,&m,&d);
  qDebug( "New dateTimeEdit1's date:%ld_%ld_%ld" , y , m , d);
  //
  qDebug( "New dateTimeEdit1's time:%ld_%ld_%ld" ,
    ui->dateTimeEdit1->time().hour(),
    ui->dateTimeEdit1->time().minute(),
    ui->dateTimeEdit1->time().second() );

}

void MainWindow::onDateTimeEdit2dateTimeChanged(const QDateTime &datetime)
{
  int y,m,d;
  ui->dateTimeEdit2->date().getDate(&y,&m,&d);
  qDebug( "New dateTimeEdit2's date:%ld_%ld_%ld" , y , m , d);
  //
  qDebug( "New dateTimeEdit2's time:%ld_%ld_%ld" ,
    ui->dateTimeEdit2->time().hour(),
    ui->dateTimeEdit2->time().minute(),
    ui->dateTimeEdit2->time().second() );

}

