//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  connect( ui->spinBox , QOverload<int>::of( &QSpinBox::valueChanged ), this , &MainWindow::onSpinBoxValueChanged );
  //
  ui->spinBox->setValue( 50 );
  //
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onSpinBoxValueChanged(int value)
{
  qDebug( "new value:%ld" , ui->spinBox->value() );
}

