//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  connect( ui->doubleSpinBox , QOverload<double>::of(&QDoubleSpinBox::valueChanged) , this , &MainWindow::onDoubleSpinBoxValueChanged );
  //
  ui->doubleSpinBox->setValue( 50.5 );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onDoubleSpinBoxValueChanged(double vlaue)
{
  qDebug( "new value:%.2lf" , ui->doubleSpinBox->value() );
}

