//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  connect(ui->pushButton,&QPushButton::clicked,this,&MainWindow::onPushButtonClicked);
  //
  ui->textEdit->setText(tr("Pleas input something") );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onPushButtonClicked(bool clicked)
{
  qDebug("TextEdit:%s",ui->textEdit->toPlainText().toLocal8Bit().data() );
}

