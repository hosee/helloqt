//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //manual add
  //Don't use follow
  //QMenu* pMyMenu = (QMenu*)ui->menubar->addMenu( new QMenu( "MyMenu" ) );
  QMenu* pMyMenu = ui->menubar->addMenu( new QMenu( "MyMenu" ) )->menu();
  QAction* pMyAction = new QAction( "MyAction" );
  pMyMenu->addAction( pMyAction) ;

  //Get actions
  //Don't use follow
  //QMenu* pOption = (QMenu*)ui->menubar->actions().at(0);
  QMenu* pOption = ui->menubar->actions().at(0)->menu();
  qDebug( "%s" , pOption->title().toLocal8Bit().data() );
  //
  QMenu* pGroup = pOption->actions().at(0)->menu();
  qDebug( "%s" , pGroup->title().toLocal8Bit().data() );
  //
  qDebug( "%s" , pGroup->actions().at(0)->text().toLocal8Bit().data() );
  qDebug( "%s" , pGroup->actions().at(1)->text().toLocal8Bit().data() );
  qDebug( "%s" , pOption->actions().at(1)->text().toLocal8Bit().data() );

  //Bind event
  connect( pGroup->actions().at(0) , &QAction::triggered , this , &MainWindow::onAction1Triggered);
  connect( pGroup->actions().at(1) , &QAction::triggered , this , &MainWindow::onAction2Triggered);
  connect( pOption->actions().at(1) , &QAction::triggered , this , &MainWindow::onAction3Triggered);
  connect( pMyAction , &QAction::triggered , this , &MainWindow::onMyActionTriggered);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onAction1Triggered(bool checked)
{
  qDebug("Action1 triggered");
}

void MainWindow::onAction2Triggered(bool checked)
{
  qDebug("Action2 triggered");
}

void MainWindow::onAction3Triggered(bool checked)
{
  qDebug("Action3 triggered");
}

void MainWindow::onMyActionTriggered(bool checked)
{
  qDebug("MyAction triggered");
}

