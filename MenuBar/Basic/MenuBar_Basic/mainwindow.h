//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

protected slots:
  void onAction1Triggered(bool checked);
  void onAction2Triggered(bool checked);
  void onAction3Triggered(bool checked);
  void onMyActionTriggered(bool checked);
private:
  Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
