//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  connect( ui->progressBar , &QProgressBar::valueChanged , this , &MainWindow::onProgressBarValueChanged );
  //
  ui->progressBar->setValue(851);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onProgressBarValueChanged(int value)
{
  qDebug( "New value:%ld" , ui->progressBar->value() );
  //
  int newPersent =  ( ( ui->progressBar->value() - ui->progressBar->minimum() ) * 100) / (ui->progressBar->maximum() - ui->progressBar->minimum() );
  qDebug( "New persent:%ld" , newPersent );
}

