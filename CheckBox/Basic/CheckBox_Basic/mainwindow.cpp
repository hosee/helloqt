//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  connect( ui->pushButton , &QPushButton::clicked , this , &MainWindow::onPushButtonClicked );
  connect( ui->checkBox1 , &QCheckBox::stateChanged , this , &MainWindow::onCheckBox1StateChanged );
  //
  ui->checkBox1->setChecked( true );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onPushButtonClicked( bool clicked )
{
  qDebug( "Option1:%s" , ui->checkBox1->isChecked() ? "Y" : "N" );
  qDebug( "Option2:%s" , ui->checkBox2->isChecked() ? "Y" : "N" );
  qDebug( "Option3:%s" , ui->checkBox3->isChecked() ? "Y" : "N" );
}

void MainWindow::onCheckBox1StateChanged(int state)
{
  qDebug( "Option1 state changed" );
}

