
# This Python file uses the following encoding: utf-8
from PyQt5 import QtCore , QtWidgets , uic
from PyQt5.QtCore import pyqtSlot , qDebug

class MainWindow( QtWidgets.QMainWindow ):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)
        super().__init__()
        uic.loadUi("mainwindow.ui", self)
        #BindEvent
        self.pushButton.clicked.connect( self.onPushButtonClicked )
    @pyqtSlot()
    def onPushButtonClicked(self):
        qDebug( 'Hello' )


