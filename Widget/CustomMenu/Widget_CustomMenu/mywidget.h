//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>

#include <QPainter>
#include <QMenu>
#include <QAction>
class MyWidget : public QWidget
{
  Q_OBJECT
public:
  explicit MyWidget(QWidget *parent = nullptr);

protected:
  void paintEvent(QPaintEvent *event) override;
  void onTreeViewCustomMenu(const QPoint &pos);
  void onTreeViewAction1Trigger();
  void onTreeViewAction2Trigger();
  void onTreeViewAction3Trigger();
signals:
  //
private:
  QMenu* m_pTreeViewCustomMenu;
};

#endif // MYWIDGET_H
