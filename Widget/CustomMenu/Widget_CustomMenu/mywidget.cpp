//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mywidget.h"

MyWidget::MyWidget(QWidget *parent) : QWidget(parent)
{
  this->setContextMenuPolicy(Qt::CustomContextMenu);
  connect( this , &QWidget::customContextMenuRequested , this , &MyWidget::onTreeViewCustomMenu );
  //Set up custom menu
  m_pTreeViewCustomMenu = new QMenu( this );
  QAction* pAct1 = new QAction( "Action 1" , this );
  connect( pAct1 , &QAction::triggered , this , &MyWidget::onTreeViewAction1Trigger );
  m_pTreeViewCustomMenu->addAction( pAct1 );
  QAction* pAct2 = new QAction( "Action 2" , this );
  connect( pAct2 , &QAction::triggered , this , &MyWidget::onTreeViewAction2Trigger );
  m_pTreeViewCustomMenu->addAction( pAct2 );
  QAction* pAct3 = new QAction( "Action 3" , this );
  connect( pAct3 , &QAction::triggered , this , &MyWidget::onTreeViewAction3Trigger );
  m_pTreeViewCustomMenu->addAction( pAct3 );
}

void MyWidget::paintEvent(QPaintEvent *event)
{
  QPainter painter( this );
  QPen pen( Qt::black );
  pen.setWidth( 4 );
  painter.setPen( pen );
  QRect rc( 4 , 4 , this->width() - 8, this->height() - 8 );
  painter.drawRect( rc );

}

void MyWidget::onTreeViewCustomMenu(const QPoint &pos)
{
  //
  m_pTreeViewCustomMenu->popup( this->mapToGlobal( pos ) );
}

void MyWidget::onTreeViewAction1Trigger()
{
  qDebug( "Action1 triggered" );
}
void MyWidget::onTreeViewAction2Trigger()
{
  qDebug( "Action2 triggered" );
}
void MyWidget::onTreeViewAction3Trigger()
{
  qDebug( "Action3 triggered" );
}
