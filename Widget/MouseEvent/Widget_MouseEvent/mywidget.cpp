//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mywidget.h"

MyWidget::MyWidget(QWidget *parent) : QWidget(parent)
{
}

void MyWidget::paintEvent(QPaintEvent *event)
{
  QPainter painter( this );
  QPen pen( Qt::black );
  pen.setWidth( 4 );
  painter.setPen( pen );
  QRect rc( 4, 4, this->width() - 8, this->height() - 8 );

  painter.drawRect( rc );

}

void MyWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
  qDebug( "mouseDoubleClickEvent" );
}

void MyWidget::mouseMoveEvent(QMouseEvent *event)
{
  qDebug( "mouseMoveEvent" );
}

void MyWidget::mousePressEvent(QMouseEvent *event)
{
  qDebug( "mousePressEvent" );
  //
  switch( event->button() )
  {
  case Qt::LeftButton:
    qDebug( "Left button clicked" );
    break;
  case Qt::MidButton:
    qDebug( "Middle button clicked" );
    break;
  case Qt::RightButton:
    qDebug( "Right button clicked" );
    break;
  default:
    qDebug( "Unknown button clicked" );
    break;
  }
  //
  qDebug( "PoxX:%ld PosY:%ld" ,event->x() , event->y() );
}

void MyWidget::mouseReleaseEvent(QMouseEvent *event)
{
  qDebug( "mouseReleaseEvent" );
}

void MyWidget::wheelEvent(QWheelEvent *event)
{
  qDebug( "wheelEvent" );
  qDebug( "wheel:%ld" , event->delta() );

}
