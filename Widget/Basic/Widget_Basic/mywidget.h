//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#ifndef MYWIDGET_H
#define MYWIDGET_H

#include <QWidget>

#include <QPainter>
class mywidget : public QWidget
{
  Q_OBJECT
public:
  explicit mywidget(QWidget *parent = nullptr);

protected:
  void paintEvent(QPaintEvent *event) override;
signals:

};

#endif // MYWIDGET_H
