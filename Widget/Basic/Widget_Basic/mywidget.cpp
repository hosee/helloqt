//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mywidget.h"

mywidget::mywidget(QWidget *parent) : QWidget(parent)
{

}

void mywidget::paintEvent(QPaintEvent *event)
{
  QPainter painter( this );
  QPen pen( Qt::black );
  pen.setWidth( 4 );
  painter.setPen( pen );
  QRect rc( 4, 4, 112, 112 );
  painter.drawRect( rc );

}
