//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#ifndef DIECT3D10WIDGET_H
#define DIECT3D10WIDGET_H

#include <QWidget>
#include <QTimer>
#include <QEvent>
//
#include <exception>
//
#include <d3d10.h>
#include <DirectXMath.h>
#include <D3Dcompiler.h>

class Direct3D10Widget : public QWidget
{
  Q_OBJECT
public:
  explicit Direct3D10Widget(QWidget *parent = nullptr);
  ~Direct3D10Widget();
  void release();
private:
  bool event(QEvent * event) override;
  void showEvent(QShowEvent * event) override;
  QPaintEngine *paintEngine() const override;
  void paintEvent(QPaintEvent * event) override;
  void resizeEvent(QResizeEvent * event) override;
signals:
  void widgetResized();
  void rendered();
  void d3dReady();
private Q_SLOTS:
  void onFrame();
  void onReset();
private:
  bool m_bIsTimerStart;
  QTimer m_cFrameTimer;
  //
  ID3D10Device* m_pDevice;
  IDXGISwapChain* m_pSwapChain;
  ID3D10RenderTargetView* m_pRTView;

};

#endif // DIECT3D10WIDGET_H
