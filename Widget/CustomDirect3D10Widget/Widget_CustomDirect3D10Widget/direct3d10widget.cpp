//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "direct3d10widget.h"
#pragma comment(lib, "d3d10.lib")
Direct3D10Widget::Direct3D10Widget(QWidget *parent) :
  QWidget(parent),
  m_bIsTimerStart(false),
  m_pDevice(nullptr),
  m_pSwapChain(nullptr),
  m_pRTView(nullptr)
{
  QPalette pal = palette();
  pal.setColor( QPalette::Background, Qt::black );
  setAutoFillBackground( true );
  setPalette( pal );
  //
  setFocusPolicy( Qt::StrongFocus );
  setAttribute( Qt::WA_NativeWindow );
  //
  setAttribute( Qt::WA_PaintOnScreen );
  setAttribute( Qt::WA_NoSystemBackground );
}
Direct3D10Widget::~Direct3D10Widget()
{
  release();
}
void Direct3D10Widget::release()
{
  if( m_bIsTimerStart )
  {
    disconnect( &m_cFrameTimer , &QTimer::timeout, this , &Direct3D10Widget::onFrame );
    m_cFrameTimer.stop();
    m_bIsTimerStart = false;
    //
    if( m_pRTView != nullptr )
    {
      m_pRTView->Release();
      m_pRTView = nullptr;
    }
    if( m_pSwapChain != nullptr )
    {
      m_pSwapChain->Release();
      m_pSwapChain = nullptr;
    }
    if( m_pDevice != nullptr )
    {
      m_pDevice->Release();
      m_pDevice = nullptr;
    }
  }
}
bool Direct3D10Widget::event(QEvent *event)
{
  return QWidget::event(event);
}
void Direct3D10Widget::showEvent(QShowEvent *event)
{
  if(!m_bIsTimerStart)
  {
    //
    DXGI_SWAP_CHAIN_DESC sd = {};
    sd.BufferCount = 2;
    sd.BufferDesc.Width = width();
    sd.BufferDesc.Height = height();
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = reinterpret_cast<HWND>(winId());
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;
    sd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;

    UINT iCreateFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
    iCreateFlags |= D3D10_CREATE_DEVICE_DEBUG;
#endif
    auto hr = D3D10CreateDeviceAndSwapChain(NULL, D3D10_DRIVER_TYPE_HARDWARE,
                                         NULL, iCreateFlags,
                                         D3D10_SDK_VERSION, &sd,
                                         &m_pSwapChain, &m_pDevice);
    if (FAILED(hr))
    {
      throw std::runtime_error("D3D10CreateDeviceAndSwapChain() has error");
    }
    //
    onReset();
    emit d3dReady();
    //
    connect(&m_cFrameTimer, &QTimer::timeout, this, &Direct3D10Widget::onFrame);
    m_cFrameTimer.start(16);
    m_bIsTimerStart=true;
  }
  //
  QWidget::showEvent(event);
}

QPaintEngine *Direct3D10Widget::paintEngine() const
{
  return nullptr;
}

void Direct3D10Widget::paintEvent(QPaintEvent *event)
{

}

void Direct3D10Widget::resizeEvent(QResizeEvent *event)
{
  if( m_bIsTimerStart )
  {
    onReset();
    emit widgetResized();
  }
  //
  QWidget::resizeEvent( event );
}

void Direct3D10Widget::onFrame()
{
  //
  const float clearColor[ 4 ] = { 0.0f , 0.0f , 1.0f , 1.0f  };
  m_pDevice->OMSetRenderTargets( 1, &m_pRTView, nullptr );
  m_pDevice->ClearRenderTargetView( m_pRTView , clearColor );
  //
  emit rendered();
  //
  if ( FAILED ( m_pSwapChain->Present( 1, 0 ) ) )
  {
    onReset();
  }
}

void Direct3D10Widget::onReset()
{
  ID3D10Texture2D* pBackBuffer;
  if( m_pRTView != nullptr )
  {
    m_pRTView->Release();
    m_pRTView = nullptr;
  }

  auto hr = m_pSwapChain->ResizeBuffers( 0, width() , height() , DXGI_FORMAT_UNKNOWN , 0 );
  if (FAILED( hr ) )
  {
    throw std::runtime_error("ResizeBuffers() has error");
  }
  hr = m_pSwapChain->GetBuffer( 0, IID_PPV_ARGS( &pBackBuffer ) );
  if (FAILED( hr ) )
  {
    throw std::runtime_error("GetBuffer() has error");
  }
  hr = m_pDevice->CreateRenderTargetView( pBackBuffer , nullptr , &m_pRTView );
  if (FAILED( hr ) )
  {
    throw std::runtime_error("CreateRenderTargetView() has error");
  }
  //
  if( pBackBuffer != nullptr )
  {
    pBackBuffer->Release();
    pBackBuffer = nullptr;
  }
  //
  qDebug("wiget resize");
}
