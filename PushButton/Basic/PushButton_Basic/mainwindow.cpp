#include "mainwindow.h"
#include "ui_mainwindow.h"
//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //Bind event
  connect(ui->pushButton,&QPushButton::clicked,this,&MainWindow::onPushButton_Clicked);
}

MainWindow::~MainWindow()
{
  //Unbind event
  disconnect(ui->pushButton,&QPushButton::clicked,this,&MainWindow::onPushButton_Clicked);
  //
  delete ui;
}

void MainWindow::onPushButton_Clicked(bool checked)
{
  qDebug("Hello Qt");
}

