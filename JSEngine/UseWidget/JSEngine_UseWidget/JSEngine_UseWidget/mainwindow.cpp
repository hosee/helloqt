//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  connect( ui->runButton , &QPushButton::clicked , this , &MainWindow::onRunButtonClicked );
  //
  m_cJSEngine.installExtensions( QJSEngine::ConsoleExtension );
  //
  m_cJSEngine.globalObject().setProperty( "myButton" , m_cJSEngine.newQObject( ui->myButton ) );
  //Default code
  ui->textEdit->setText( tr(
    "myButton.text = 'Hello myButton.';\n"\
    "myButton.clicked.connect( function( clicked ){\n"\
    "  console.log('myButton clicked!');\n"\
    "});"
  ) );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onRunButtonClicked(bool clicked)
{
  QJSValue result = m_cJSEngine.evaluate( ui->textEdit->toPlainText() );
  if( result.isError() )
  {
    //
    QJSValue errorStr = result.property( tr("stack") );
    //Filter format string "stack:@:"
    QString errorLine = errorStr.toString().mid(8);
    //
    qDebug( "Error at line(%s) %s" , errorLine.toLocal8Bit().data() ,result.toString().toLocal8Bit().data() );
  }
}

