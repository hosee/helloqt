//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  connect( ui->pushButton , &QPushButton::clicked , this , &MainWindow::onPushButtonClicked );
  //
  m_cJSEngine.installExtensions( QJSEngine::ConsoleExtension );
  //
  ui->textEdit->setText( tr("console.log('Hello world')") );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onPushButtonClicked(bool clicked)
{
  QJSValue result = m_cJSEngine.evaluate( ui->textEdit->toPlainText() );
  if( result.isError() )
  {
    //
    QJSValue errorStr = result.property( tr("stack") );
    //Filter format string "stack:@:"
    QString errorLine = errorStr.toString().mid(8);
    //
    qDebug( "Error at line(%s) %s" , errorLine.toLocal8Bit().data() ,result.toString().toLocal8Bit().data() );
  }

}

