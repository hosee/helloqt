//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  connect( ui->runButton , &QPushButton::clicked , this , &MainWindow::onRunButtonClicked );
  //
  m_cJSEngine.installExtensions( QJSEngine::ConsoleExtension );
  //
  MyClass* pMyClass = new MyClass();
  m_cJSEngine.globalObject().setProperty( "myClass" ,m_cJSEngine.newQObject( pMyClass ) );
  //
  ui->textEdit->setText( tr(
    "console.log( myClass.dataInt );\n"\
    "console.log( myClass.dataDouble );\n"\
    "console.log( myClass.dataString );\n"\
    "myClass.dataInt = 5678;\n"\
    "myClass.dataDouble = 1.414;\n"\
    "myClass.dataString = 'My string';\n"\
    "console.log( myClass.dataInt );\n"\
    "console.log( myClass.dataDouble );\n"\
    "console.log( myClass.dataString );\n"\
    "myClass.callMethod();\n"\
  ) );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onRunButtonClicked(bool clicked)
{
  QJSValue result = m_cJSEngine.evaluate( ui->textEdit->toPlainText() );
  if( result.isError() )
  {
    //
    QJSValue errorStr = result.property( tr("stack") );
    //Filter format string "stack:@:"
    QString errorLine = errorStr.toString().mid(8);
    //
    qDebug( "Error at line(%s) %s" , errorLine.toLocal8Bit().data() ,result.toString().toLocal8Bit().data() );
  }

}
