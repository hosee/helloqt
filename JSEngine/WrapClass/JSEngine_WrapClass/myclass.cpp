//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "myclass.h"

MyClass::MyClass(QObject *parent) :
  QObject(parent),
  m_MyInt( 1234 ),
  m_MyDouble( 3.14159 )
{
  m_MyString = "Hello";
}
void MyClass::setMyInt( int value )
{
  m_MyInt = value;
}
int MyClass::myInt() const
{
  return m_MyInt;
}
void MyClass::setMyDouble( double value )
{
  m_MyDouble = value;
}
double MyClass::myDouble() const
{
  return m_MyDouble;
}
void MyClass::setMyString( const QString& value )
{
  m_MyString = value;
}
QString MyClass::myString() const
{
  return m_MyString;
}

void MyClass::callMethod()
{
  qDebug( "MyClass::callMethod()" );
}
