//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#ifndef MYCLASS_H
#define MYCLASS_H

#include <QObject>

class MyClass : public QObject
{
  Q_OBJECT
  Q_PROPERTY(int dataInt READ myInt WRITE setMyInt)
  Q_PROPERTY(double dataDouble READ myDouble WRITE setMyDouble)
  Q_PROPERTY(QString dataString READ myString WRITE setMyString)
public:
  explicit MyClass(QObject *parent = nullptr);
//
  void setMyInt( int value );
  int myInt() const;
  void setMyDouble( double value );
  double myDouble() const;
  void setMyString( const QString& value );
  QString myString() const;
  Q_INVOKABLE void callMethod();
signals:
private:
  int m_MyInt;
  double m_MyDouble;
  QString m_MyString;
};

#endif // MYCLASS_H
