//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
, m_pTreeViewCustomMenu( NULL )
{
  ui->setupUi(this);
  //
  QStandardItemModel* pModel = new QStandardItemModel(ui->treeView);
  pModel->setHorizontalHeaderLabels( QStringList()<<QStringLiteral("Name")<<QStringLiteral("Comment") );
  QStandardItem* pItemRoot1 = new QStandardItem(QStringLiteral("root1") );
  //first
  pItemRoot1->setChild( 0 ,0 ,new QStandardItem(QStringLiteral("child") ) );
  pItemRoot1->setChild( 0 ,1 ,new QStandardItem(QStringLiteral("I'm child") ) );
  //
  pModel->appendRow( pItemRoot1 );
  //
  ui->treeView->setModel( pModel );

  //
  ui->treeView->setContextMenuPolicy( Qt::CustomContextMenu );

  //
  connect( ui->treeView , &QTreeView::customContextMenuRequested , this , &MainWindow::onTreeViewCustomMenu );

  //Set up custom menu
  m_pTreeViewCustomMenu = new QMenu( this );
  QAction* pAct1 = new QAction( "Action 1" , this );
  connect( pAct1 , &QAction::triggered , this , &MainWindow::onTreeViewAction1Trigger );
  m_pTreeViewCustomMenu->addAction( pAct1 );
  QAction* pAct2 = new QAction( "Action 2" , this );
  connect( pAct2 , &QAction::triggered , this , &MainWindow::onTreeViewAction2Trigger );
  m_pTreeViewCustomMenu->addAction( pAct2 );
  QAction* pAct3 = new QAction( "Action 3" , this );
  connect( pAct3 , &QAction::triggered , this , &MainWindow::onTreeViewAction3Trigger );
  m_pTreeViewCustomMenu->addAction( pAct3 );
}

MainWindow::~MainWindow()
{
  delete m_pTreeViewCustomMenu;
  delete ui;
}
void MainWindow::onTreeViewCustomMenu( const QPoint& point )
{
  //
  m_pTreeViewCustomMenu->popup( ui->treeView->viewport()->mapToGlobal( point ) );
}

void MainWindow::onTreeViewAction1Trigger()
{
  qDebug( "Action1 triggered" );
}

void MainWindow::onTreeViewAction2Trigger()
{
  qDebug( "Action2 triggered" );
}
void MainWindow::onTreeViewAction3Trigger()
{
  qDebug( "Action3 triggered" );
}
