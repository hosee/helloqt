//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  QStandardItemModel* pModel = new QStandardItemModel(ui->treeView);
  pModel->setHorizontalHeaderLabels( QStringList()<<QStringLiteral( "Name" )<<QStringLiteral( "Comment" ) );
  QStandardItem* pItemRoot1 = new QStandardItem( QStringLiteral( "root1" ) );
  //first
  pItemRoot1->setChild( 0 , 0 , new QStandardItem( QStringLiteral( "child" ) ) );
  pItemRoot1->setChild( 0 , 1 , new QStandardItem( QStringLiteral( "I'm child" ) ) );
  //second
  pItemRoot1->setChild( 1 , 0 , new QStandardItem( QStringLiteral( "child1" ) ) );
  //
  pModel->appendRow( pItemRoot1 );

  //
  QStandardItem* pItemRoot2 = new QStandardItem( QStringLiteral( "root2" ) );
  pItemRoot2->setChild( 0 , 0 , new QStandardItem( QStringLiteral( "child" ) ) );
  pItemRoot2->setChild( 0 , 1 , new QStandardItem( QStringLiteral( "I'm child" ) ) );
  //
  pModel->appendRow( pItemRoot2 );

  //
  ui->treeView->setModel( pModel );
  //event bind
  connect( ui->addRootItemButton , &QPushButton::clicked , this , &MainWindow::onAddRootItemButtonClicked );
  connect( ui->addChildItemButton , &QPushButton::clicked , this , &MainWindow::onAddChildItemButtonClicked );
  connect( ui->removeButton , &QPushButton::clicked , this , &MainWindow::onRemoveButtonClicked );
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onAddRootItemButtonClicked( bool enabled )
{
  QStandardItemModel* pModel = (QStandardItemModel*)ui->treeView->model();
  pModel->appendRow( new QStandardItem( QStringLiteral( "add root item" ) ) );
}

void MainWindow::onAddChildItemButtonClicked( bool enabled )
{
  QModelIndex index = ui->treeView->currentIndex();
  QStandardItemModel* pModel = (QStandardItemModel*)ui->treeView->model();
  if( index.isValid() )
  {
    QStandardItem* pSelectedItem = pModel->itemFromIndex( index );
    pSelectedItem->appendRow( new QStandardItem( QStringLiteral( "add child item" ) ) );
  }
}

void MainWindow::onRemoveButtonClicked( bool enabled )
{
  QModelIndex index = ui->treeView->currentIndex();
  QStandardItemModel* pModel = (QStandardItemModel*)ui->treeView->model();
  if(index.isValid() )
  {
    pModel->removeRow( index.row() , index.parent() );
  }
}

