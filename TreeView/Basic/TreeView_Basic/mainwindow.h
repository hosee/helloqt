//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStandardItemModel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
protected slots:
  void onAddRootItemButtonClicked( bool enabled );
  void onAddChildItemButtonClicked( bool enabled );
  void onRemoveButtonClicked( bool enabled );
private:
  Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
