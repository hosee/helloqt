//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mytreeview.h"

MyTreeView::MyTreeView(QWidget *parent):
  QTreeView(parent)
{
  this->setDragEnabled( true );
  this->setDragDropMode( QAbstractItemView::InternalMove );
  //
  this->setHeaderHidden( true );
  this->setSelectionMode( QAbstractItemView::ExtendedSelection );
  //
  m_cHideColumnList.push_back( 1 );
}

void MyTreeView::CustomSetModel(QStandardItemModel *pModel)
{
  this->setModel( pModel );
  HideColumnByList();
}

void MyTreeView::HideColumnByList()
{
  for( auto i = 0 ; i < m_cHideColumnList.size() ; i++ )
  {
    this->hideColumn( m_cHideColumnList[i] );
  }
}
void MyTreeView::dragEnterEvent(QDragEnterEvent *event)
{
  qDebug( "dragEnterEvent" );
  //
  QTreeView::dragEnterEvent( event );
}

void MyTreeView::dragMoveEvent(QDragMoveEvent *event)
{
  qDebug( "dragMoveEvent" );
  //
  QTreeView::dragMoveEvent( event );
}

void MyTreeView::dragLeaveEvent(QDragLeaveEvent *event)
{
  qDebug( "dragLeaveEvent" );
  //
  QTreeView::dragLeaveEvent( event );
}
void MyTreeView::dropEvent(QDropEvent *event)
{
  qDebug( "dropEvent" );
  QModelIndex index = indexAt(event->pos() );
  qDebug( "dropEvent node:%s" , index.data().toString().toLocal8Bit().data() );

  DropIndicatorPosition dropIndicator = dropIndicatorPosition();
  switch ( dropIndicator ) {
  case DropIndicatorPosition::OnItem:
    qDebug("onItem");
    break;
  case DropIndicatorPosition::AboveItem:
    qDebug("AboveItem");
    break;
  case DropIndicatorPosition::BelowItem:
    qDebug("BelowItem");
    break;
  defadefault:
    qDebug("Unknown DropIndicatorPosition!");
    break;
  }
  //
  for( auto i = 0 ; i < this->selectedIndexes().size() ; i++ )
  {
    if( this->selectedIndexes().at(i).column() != 0 )
      continue;
    //
    qDebug( "source node:%s" , this->selectedIndexes().at(i).data().toString().toLocal8Bit().data() );
  }
  //
  HideColumnByList();
  //
  QTreeView::dropEvent(event);
}
