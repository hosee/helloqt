//*****************************************************************************
//
// Copyright (c) Hosee. All Rights Reserved.
// This code is licensed under the MIT License (MIT).
//
//*****************************************************************************

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent)
, ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  //
  ui->comboBox->addItem("MyAddOption");
  ui->comboBox->insertItem(2,"MyInsertOption");
  //
  connect( ui->pushButton , &QPushButton::clicked , this , &MainWindow::onPushButton_Clicked );
  connect( ui->comboBox , QOverload<int>::of( &QComboBox::currentIndexChanged ) , this , &MainWindow::onComboBoc_CurrentIndexChanged );
  //Force select option by index
  //ui->comboBox->setCurrentIndex(1);

  //Force select option by string
  //ui->comboBox->setCurrentText("Option1");
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onPushButton_Clicked(bool clicked)
{
  qDebug( "Result text:%s" , ui->comboBox->currentText().toLocal8Bit().data() );
  qDebug( "Result index:%ld" , ui->comboBox->currentIndex() );

}

void MainWindow::onComboBoc_CurrentIndexChanged(int index)
{
  qDebug( "Select text:%s" , ui->comboBox->currentText().toLocal8Bit().data() );
  qDebug( "Select index:%ld" , ui->comboBox->currentIndex() );
}

